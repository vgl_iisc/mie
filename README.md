#MIE: Multifield Isosurface Explorer#

Identifcation of important isovalues of scalar felds is a well studied problem. In this application we have used a relation-aware approach to explore scalar multifeld data. In the context of multifeld data, we calculate a variation density function, whose profle is a good indicator of interesting isovalues of individual scalar felds. The application, includes a graph viewer to plot the variation density function against isovalues and an Isosurface viewer to interact with Isosurfaces. The implementation is based on ideas described in [1]. 

* Source code on GitHub is available  at https://github.com/baali/Multifield-Isosurface-Explorer.

### Contributors ###

* Suthambara
* Shantanu

### References ###

1. Suthambhara N. and Vijay Natarajan.
Relation-aware isosurface extraction in multi-field data. 
IEEE Transactions on Visualization and Computer Graphics, 17(2), 2011, 182-191.